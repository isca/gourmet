FROM      golang:1.12-alpine3.9 as builder
ENV       app="gourmet"
WORKDIR   /go/src
ADD       ./ ./"${app}"
RUN       (cd $app;export GO111MODULE=on;go build -mod vendor -tags netgo -a -installsuffix cgo -o ${app} .)

FROM      alpine:latest 
LABEL     maintainer="isca <at isca dot space>"
ENV       app="gourmet"
ENV       sshfile="/etc/ssh/sshd_config"
RUN       apk add --add --no-cache ca-certificates shadow openssh \
            && rm -rf /tmp/* /var/cache/apk/*
RUN       (mkdir /var/run/sshd \
          && chmod 700 /var/run \
          && groupadd "${app}" \
          && useradd -m -d /home/"${app}" -g "${app}" -s /home/"${app}"/"${app}" "${app}" \
          && usermod -p '' ${app} \
          && sed -i "s,^#PasswordAuthentication.*\$,PasswordAuthentication\ yes," ${sshfile} \
          && sed -i "s,^#PermitEmptyPasswords.*\$,PermitEmptyPasswords\ yes," ${sshfile} \
          && sed -i "s,^${app}:x:,${app}::," /etc/passwd \
          && ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa \ 
          && ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa \ 
          && mkdir -p /var/run/sshd \
          && touch /var/log/ssh)
COPY      --from=builder /go/src/"${app}"/"${app}" /home/"${app}"/"${app}"
EXPOSE    22
CMD       ["/usr/sbin/sshd", "-E", "/var/log/ssh", "-D"]
