package main

/* *** JogoGourmet(gourmet) ***
This is a simple game to interact with users asking for food plates and
keeping the results only in memmory. The original version came to me
from a hiring challenge of a company called objective.
The main idea here is to play from terminal, but I'll try in my spare
time to improve it and convert to an webassembly app.
Author: Isca(igorsca at protonmail dot com)
Ctba: 13-05-19
*/

import (
	"gourmet/cmd"
	"strings"
)

func main() {

	plate := cmd.PlateBuilder("Lasanha")
	pasta := cmd.ChrBuilder("massa", plate)
	deserve := cmd.PlateBuilder("Bolo de Chocolate")
	others := cmd.ChrBuilder("", deserve)
	//plate = model.Plate{Name: "macarao"}
	//pasta.Plates = append(pasta.Plates, plate)

mainloop:
	for {
		cmd.Ask("Pense em um prato que voce gosta!%s", "")
		reply := cmd.Ask("Voce esta pensando em %s? (Sim ou Nao): ", pasta.Name)
		switch strings.ToLower(reply) {
		case "sim":
			pasta = cmd.ChrsDecision(pasta)
			continue mainloop
		case "nao":
			others = cmd.ChrsDecision(others)
			continue mainloop

		}
	}
}
