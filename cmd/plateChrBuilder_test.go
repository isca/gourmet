package cmd

import (
	"gourmet/model"
	"testing"
)

func TestPlateBuilder(t *testing.T) {
	plate := model.Plate{Name: "macarrao"}
	tt := []struct {
		name string
		model.Plate
	}{
		{plate.Name, plate},
	}
	for _, tc := range tt {
		res := PlateBuilder(tc.name)
		if res.Name != plate.Name {
			t.Errorf("failed, expected:%s, received:%s", plate.Name, res)
		}
	}
}

func TestChrBuilder(t *testing.T) {
	plate := model.Plate{Name: "macarrao"}
	chr := model.Chr{
		Name: plate.Name,
		Plates: []model.Plate{
			plate,
		},
	}
	tt := []struct {
		name string
		model.Plate
		model.Chr
	}{
		{plate.Name, plate, chr},
	}
	for _, tc := range tt {
		res := ChrBuilder(tc.name, plate)
		if res.Name != chr.Name {
			t.Errorf("failed, expected:%s, received:%s", chr.Name, res)
		}
	}
}

func TestSubChrBuilder(t *testing.T) {
	plate := model.Plate{Name: "macarrao"}
	chr := model.Chr{
		Name: plate.Name,
		Plates: []model.Plate{
			plate,
		},
	}
	tt := []struct {
		name string
		model.Plate
		model.Chr
	}{
		{plate.Name, plate, chr},
	}
	for _, tc := range tt {
		res := SubChrBuilder(tc.name, chr)
		if res.Name != chr.Name {
			t.Errorf("failed, expected:%s, received:%s", chr.Name, res)
		}
	}
}

func TestSubPlateBuilder(t *testing.T) {
	plate := []model.Plate{{Name: "macarrao"}}
	tt := []struct {
		name string
		p    []model.Plate
	}{
		{plate[0].Name, plate},
	}
	for _, tc := range tt {
		chr := model.Chr{
			Name:   tc.name,
			Plates: tc.p,
			Chrs: []model.Chr{
				{
					Name: tc.name,
					Plates: []model.Plate{
						{
							Name: tc.name,
						},
					},
				},
			},
		}
		res := SubPlateBuilder(0, tc.name, chr)
		if res.Name != chr.Name {
			t.Errorf("failed, expected:%s, received:%s", chr.Name, res)
		}
	}
}
