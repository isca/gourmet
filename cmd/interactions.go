package cmd

import (
	"bufio"
	"fmt"
	"gourmet/model"
	"os"
	"strings"
)

/* *** cmd ***
Here we have all the interactions and logic decisions.
Author: Isca(igorsca at protonmail dot com)
Ctba: 13-05-19
*/

// Ask creates the dialogs to interact with users.
func Ask(m string, p interface{}) (r string) {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Printf(m, p)
	scanner.Scan()
	r = scanner.Text()
	return
}

// AskForNewPlate use cmd constructors to append the Chr struct by resulting in a new Plate.
func askForNewPlate(n string, c model.Chr) (ch model.Chr) {
	answerPlate := Ask("Em qual prato voce esta pensando?:%s", "")
	answerChr := Ask("E oque "+answerPlate+" tem que %s nao tem?: ", n)
	plate := PlateBuilder(answerPlate)
	chr := ChrBuilder(answerChr, plate)
	c.Chrs = append(c.Chrs, chr)
	ch = c
	return
}

// plateDecision get the plate answer from the user and treat by ending or creating more plates.
func plateDecision(reply, name string, c model.Chr) (ch model.Chr) {
	switch strings.ToLower(reply) {
	case "sim":
		fmt.Println("Acertei de novo!")
	case "nao":
		ch = askForNewPlate(name, c)
		return ch
	}
	ch = c
	return ch
}

// ChrsDecision offers to the user the main or inner characteristic(kind/family/directory)
// and treat the answer by creating new characteristics.
func ChrsDecision(plate model.Chr) (pl model.Chr) {
	if len(plate.Chrs) > 0 {
		for _, c := range plate.Chrs {
			reply := Ask("Voce esta pensando em %s? (Sim ou Nao): ", c.Name)
			switch strings.ToLower(reply) {
			case "sim":
				for _, p := range c.Plates {
					reply = Ask("Seu prato eh %s? (Sim ou Nao): ", p.Name)
					plate = plateDecision(reply, p.Name, plate)
					pl = plate
					return pl
				}
			}

		}
	}
	for _, p := range plate.Plates {
		reply := Ask("Seu prato eh %s? (Sim ou Nao): ", p.Name)
		plate = plateDecision(reply, p.Name, plate)
		pl = plate
	}
	return pl
}
