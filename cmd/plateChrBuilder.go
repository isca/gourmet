package cmd

/* *** cmd ***
All the struct modelation commands.
Author: Isca(igorsca at protonmail dot com)
Ctba: 13-05-19
*/

import "gourmet/model"

// PlateBuilder populates the plate struct resulting in a new plate option.
func PlateBuilder(n string) (p model.Plate) {
	p = model.Plate{
		Name: n,
	}
	return
}

// ChrBuilder populates the (characteristics/directories) for new plates.
func ChrBuilder(n string, p model.Plate) (c model.Chr) {
	c = model.Chr{
		Name: n,
		Plates: []model.Plate{
			p,
		},
	}
	return
}

// SubChrBuilder populates the subCharacteristics
// This feature is not used, but it's here for the sake
// of the future improvement.
func SubChrBuilder(n string, c model.Chr) (ch model.Chr) {
	ch = model.Chr{
		Name:   c.Name,
		Plates: c.Plates,
		Chrs: []model.Chr{
			{
				Name: n,
			},
		},
	}
	return
}

// SubPlateBuilder populates the subPlates like a files in directory trees
// I'll keep to show the struct modelation in this code.
func SubPlateBuilder(i int, n string, c model.Chr) (ch model.Chr) {
	ch = model.Chr{
		Name:   c.Name,
		Plates: c.Plates,
		Chrs: []model.Chr{
			{
				Name: c.Chrs[i].Name,
				Plates: []model.Plate{
					{
						Name: n,
					},
				},
			},
		},
	}
	return
}
