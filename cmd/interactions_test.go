package cmd

import (
	"gourmet/model"
	"io/ioutil"
	"log"
	"os"
	"testing"
)

func TestAsk(t *testing.T) {
	tt := []struct {
		in     string
		append interface{}
		out    string
	}{
		{"%s", "append", "yes"},
	}

	for _, tc := range tt {
		content := []byte(tc.out)
		tmpfile, err := ioutil.TempFile("", "tmp")
		if err != nil {
			log.Fatal(err)
		}

		defer os.Remove(tmpfile.Name())

		if _, err := tmpfile.Write(content); err != nil {
			log.Fatal(err)
		}

		if _, err := tmpfile.Seek(0, 0); err != nil {
			log.Fatal(err)
		}

		oldStdin := os.Stdin
		defer func() { os.Stdin = oldStdin }()

		os.Stdin = tmpfile
		reply := Ask(tc.in, tc.append)
		if reply != tc.out {
			t.Errorf("failed to use Ask, expected:%s, received:%s", tc.out, reply)
		}

	}
}

func TestPlateDecision(t *testing.T) {
	tt := []struct {
		r, n string
	}{
		{"sim", "macarrao"},
		{"nao", "macarrao"},
	}
	for _, tc := range tt {
		chr := model.Chr{
			Name: tc.n,
		}
		c := plateDecision(tc.r, tc.n, chr)
		if c.Name != chr.Name {
			t.Errorf("failed to use plateDecision expect:%+v, received:%+v", chr, c)
		}
	}
}

func TestChrsDecision(t *testing.T) {
	plate := model.Plate{Name: "macarrao"}
	chr := model.Chr{
		Name: plate.Name,
		Plates: []model.Plate{
			plate,
		},
	}
	res := ChrsDecision(chr)
	if res.Name != chr.Name {
		t.Errorf("failed, expected:%s, received:%s", chr.Name, res)
	}
}
