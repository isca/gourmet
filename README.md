# Jogo Gourmet(Gourmet)
[![pipeline status](https://gitlab.com/isca/gourmet/badges/master/pipeline.svg)](https://gitlab.com/isca/gourmet/commits/master)
[![coverage report](https://gitlab.com/isca/gourmet/badges/master/coverage.svg)](https://gitlab.com/isca/gourmet/commits/master)


This project is a simple console interaction program. It work's like a simple game where you're asked something that you like  
and on each interaction you are retrofeeding the program to have more options to ask.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
See deployment for notes on how to deploy the project on a live system.

### Prerequisites

This package is built with go1.12, and all it need's is provided with the go standard library.

### Installing

What things you need to install the software and how to install them from the source:

```
go install
```

To build the docker version you can use the `Makefile`:

```
make dkbuild
```

## Running the tests

Until I finish this README there is not so much Unit tests written.  
But I will try to coverage unleass 80% of unit tests for this code as soon as possible.  

You can run tests like this:

```
go test ./...
```

To run this code locally for test purposes use:

```
go run main.go
```

_If you're trying to run this on a Windows machine, will be bether to use the docker version
since I don't have a Windows machine and this code only have been tested on MacOSX and Linux._

## Deployment

This codebase is cloud-native by design so you can use lot of environments to make this run anywhere you want.  
But to make this even more easy you to the codebase also provides a `Dockerfile` and a docker-compose.

There is also a `Makefile` to make all this even more easy.

Deploy with docker-compose:

```
docker-compose up --build
```

Run with make file:

```
make run
```

The docker version start the game on a ssh session with the user `gourmet`. So to get access to the game 
you must access the conatiner via ssh.

For e.g:
Considering the container is running on your local machine it will be accessible like this:

```
ssh gourmet@localhost -p 2244
```
Isn't fun!? :wink:


## Built With

* [go](http://golang.org/) - The GO programing language.

## Versioning

This project use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/isca/gourmet/tags).

## Authors

* **Igor Brandao** - *Initial work* - [isca](https://gitlab.com/isca)

