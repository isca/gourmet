package model

/* *** model ***
I'm not so happy with this OO like name, but here is
the structs to model this game like a directory tree.
Author: Isca(igorsca at protonmail dot com)
Ctba: 13-05-19
*/

// Plate implements an string of arrays to create more plates. (think in files)
type Plate struct {
	Name string
}

// Chr implements the characteristics for a plate. (think in directories and subdiretories)
type Chr struct {
	Name   string
	Plates []Plate
	Chrs   []Chr
}
