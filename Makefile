# 
# The default makefile for this project
# by: Isca
#
#

registry = registry.gitlab.com
project = isca/gourmet
version = master
service = gourmet

help:
	
	$(info Makefile Usage:)
	$(info build 			- build the go binary)
	$(info dkbuild 		- build a docker image for this project)
	$(info dkpush 			- push docker image to registry)
	$(info rlogin 			- gitlab registry login)
	$(info run 			- start this container)
	$(info logs 			- attach to display logs)
	$(info stop 			- stop this container)
	@printf "\n"

build:
	GO111MODULE="on"
	go build

test: 
	@(GO111MODULE="on";go test -cover ./...)

coverage:
	@(GO111MODULE="on";go test `go list ./...|grep -v /vendor/` -v -coverprofile .testCoverage.txt)

dkbuild:
ifeq ($(CI),true)
	docker build -t $(CI_REGISTRY_IMAGE):$(CI_COMMIT_TAG) .
else 
	docker build -t $(registry)/$(project):$(version) .
endif

rlogin:
ifeq ($(CI),true)
	docker login -u gitlab-ci-token -p $(CI_BUILD_TOKEN) $(CI_REGISTRY)
endif

dkpush: rlogin
ifeq ($(CI),true)
	docker push $(CI_REGISTRY_IMAGE):$(CI_COMMIT_TAG)
else 
	docker push $(registry)/$(project):$(version)
endif

run:
	docker-compose -f docker-compose.yml up --build -d

logs:
	@docker-compose logs -f

stop:
	@docker-compose down
